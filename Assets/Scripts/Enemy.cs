﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [Header("Enemy")]
    [SerializeField] private float health = 100f;
    [SerializeField] private float shotCounter;
    [SerializeField] private GameObject enemyExplosion;
    [SerializeField] private float explosionDestroyDuration = 1.0f;
    
    [Header("Shooting")]
    [SerializeField] private float minTimeBetweenShots = 0.2f;
    [SerializeField] private float maxTimeBetweenShots = 3f;
    [SerializeField] private GameObject enemyLaser;
    [SerializeField] private float projectileSpeed = 5f;
    
    [Header("SFX")]
    [SerializeField] private AudioClip enemyShootingSfx;
    [SerializeField] [Range(0, 1)] private float enemyShootingVolume = 0.1f;
    [SerializeField] private AudioClip enemyExplosionSfx;
    [SerializeField] [Range(0, 1)] private float enemyExplosionVolume = 0.7f;

    private void Start()
    {
        shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
    }

    private void Update()
    {
        CountdownAndShoot();
    }

    private void CountdownAndShoot()
    {
        shotCounter -= Time.deltaTime;
        if (shotCounter <= 0f)
        {
            Fire();
            shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
        }
    }

    private void Fire()
    {
        var position = transform.position;
        GameObject laser = Instantiate(enemyLaser, position, Quaternion.identity);
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed * -1);
        AudioSource.PlayClipAtPoint(enemyShootingSfx, Camera.main.transform.position, enemyShootingVolume);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) { return; }
        
        ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage;
        damageDealer.Hit();
        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Destroy(gameObject);
        var transform1 = transform;
        GameObject explosion = Instantiate(enemyExplosion, transform1.position, transform1.rotation);
        Destroy(explosion, explosionDestroyDuration);
        AudioSource.PlayClipAtPoint(enemyExplosionSfx, Camera.main.transform.position, enemyExplosionVolume);
    }
}
