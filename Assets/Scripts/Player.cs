﻿using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float padding = 1f;
    [SerializeField] private int health = 200;
    [SerializeField] private AudioClip playerExplodingSfx;
    [SerializeField] [Range(0, 1)] private float playerExplodingVolume = 0.7f;
    
    [Header("Projectile")]
    [SerializeField] private GameObject laserPrefab;
    [SerializeField] private float projectileSpeed = 10f;
    [SerializeField] private float projectileFiringPeriod = 0.1f;
    [SerializeField] private AudioClip playerShootingSfx;
    [SerializeField] [Range(0, 1)] private float playerShootingVolume = 0.25f;

    private float xMin;
    private float xMax;
    private float yMin;
    private float yMax;
    private IEnumerator _firingCoroutine;

    // Start is called before the first frame update
    private void Start()
    {
        _firingCoroutine = FireContinuously();
        SetupMoveBoundaries();
    }

    // Update is called once per frame
    private void Update()
    {
        Move();
        Fire();
    }

    private void SetupMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        if (!(gameCamera is null))
        {
            xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
            xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;
            yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
            yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
        }
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        
        var position = transform.position;

        var newXpos = Mathf.Clamp(position.x + deltaX, xMin, xMax);
        var newYpos = Mathf.Clamp(position.y + deltaY, yMin, yMax);

        position = new Vector2(newXpos, newYpos);
        transform.position = position;
    }

    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(_firingCoroutine);
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(_firingCoroutine);
        }
    }

    private IEnumerator FireContinuously()
    {
        while (true)
        {
            GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity) as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);
            AudioSource.PlayClipAtPoint(playerShootingSfx, Camera.main.transform.position, playerShootingVolume);
            yield return new WaitForSeconds(projectileFiringPeriod);
        }
        
        // ReSharper disable once IteratorNeverReturns
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) { return; }
        
        ProcessHit(damageDealer);
    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage;
        damageDealer.Hit();
        if (health <= 0)
        {
            PlayerDie();
        }
    }

    private void PlayerDie()
    {
        FindObjectOfType<Level>().LoadGameOver();
        Destroy(gameObject);
        AudioSource.PlayClipAtPoint(playerExplodingSfx, Camera.main.transform.position, playerExplodingVolume);
    }
}