using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Wave Config")]
public class WaveConfig : ScriptableObject
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject pathPrefab;
    [SerializeField] public float spawnTime = 0.5f;
    [SerializeField] private float spawnRandomFactor = 0.3f;
    [SerializeField] public int numberOfEnemies = 5;
    [SerializeField] public float moveSpeed = 2f;

    public GameObject GetEnemyPrefab => enemyPrefab;

    public List<Transform> GetWayPoints()
    {
        var waveWaypoints = new List<Transform>();
        foreach (Transform pathWaypoint in pathPrefab.transform)
        {
            waveWaypoints.Add(pathWaypoint);
        }
            
        return waveWaypoints;
    }

    public float GetSpawnTime => spawnTime;

    public float GetSpawnRandomFactor => spawnRandomFactor;

    public int GetNumberOfEnemies => numberOfEnemies;

    public float GetMoveSpeed => moveSpeed;
}