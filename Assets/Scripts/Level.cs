using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public void Start()
    {
        Debug.Log("started");
    }
    
    public void LoadStartMenu()
    {
        Debug.Log("loaded");
        SceneManager.LoadScene(0);
    }

    public void LoadMainGame()
    {
        Debug.Log("starting");
        SceneManager.LoadScene("Game");
    }

    public void LoadGameOver()
    {
        StartCoroutine()
        SceneManager.LoadScene("Game Over");
    }

    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
