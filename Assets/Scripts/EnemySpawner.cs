﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<WaveConfig> waveConfigs;

    [SerializeField] private int startingWave = 0;
    [SerializeField] private bool looping;
    
    IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(SpawnAllWaves());
            
        } while (looping);
    }

    private IEnumerator SpawnAllWaves()
    {
        for (int i = startingWave; i < waveConfigs.Count; i++)
        {
            var currentWave = waveConfigs[i];
            yield return StartCoroutine(SpawnAllEnemiesInWave(currentWave));
        }
    }

    private IEnumerator SpawnAllEnemiesInWave(WaveConfig currentWaveConfig)
    {
        for (int i = 0; i < currentWaveConfig.GetNumberOfEnemies; i++)
        {
            var newEnemy = Instantiate(
                currentWaveConfig.GetEnemyPrefab,
                currentWaveConfig.GetWayPoints()[0].transform.position,
                Quaternion.identity);
            
            newEnemy.GetComponent<EnemyPathing>().SetWaveConfig(currentWaveConfig);
            
            yield return new WaitForSeconds(currentWaveConfig.GetSpawnTime);
        }
    }
}